/** 
*  Transaction model
*  Describes the characteristics of each attribute in an order resource.
*
* @author Rajender Ravi Varma Devulapally <s534626@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transactionno: { type: Number, required: true, unique: true, default: 99999 },
  transactiontype: { type: String, enum: ['NA', 'credit card', 'cash', 'check'], required: true, default: 'NA' },
  transactiondate: { type: Date, required: true, default: Date },
  transactionamount: { type: Number, required: true, unique: true  },
 })

module.exports = mongoose.model('transaction', TransactionSchema)
